Devcontainer MRT
================

This repo contains the Dockerfile for the image `uprev/mrt <https://hub.docker.com/repository/docker/uprev/mrt>`_ . This contains all of the dependencies for development of the MrT framework modules including GTest Unit testing

Using Devcontainer
------------------

This repo also contains a devcontainer configuration for VS Code. To use it in your project add it as a submodule with the path `.devcontainer`

.. code:: bash 

    cd <your/project>
    git submodule add git@gitlab.com:uprev-public/devcontainers/mrt.git .devcontainer 


