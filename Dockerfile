
FROM uprev/base:ubuntu-18.04 as dev

#prevent prompts during installs
ENV DEBIAN_FRONTEND=noninteractive


ENV LC_CTYPE en_US.UTF-8
ENV LANG en_US.UTF-8


# Install system dependencies
RUN apt update -qq && apt install -qq -y --no-install-recommends \
        lcov \
        doxygen \
        libgtest-dev \
        valgrind \
        python3\ 
        python3-pip\
        gdb \
        astyle \
        git \ 
        git-lfs




# For Ubuntu Versions older than 20.04, the gtest lib must be built and installed manually for CMake integration
WORKDIR /usr/src/gtest
RUN cmake CmakeLists.txt
RUN make 
RUN make install


#Copy the latest release of plantuml. Ubuntu 18.04 package manager has an older one without timing diagram support
RUN wget https://github.com/plantuml/plantuml/releases/download/v1.2022.1/plantuml-1.2022.1.jar -O /usr/share/plantuml/plantuml.jar
